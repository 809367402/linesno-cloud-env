# zookeeper安装配置

## 本内容你将获得
- Linux上zookeeper的单点安装教程
- Linux上zookeeper的集群安装教程

## 单点安装
- 上传zookeeper3.4.6至Linux服务器`/home/linesno/zookeeper`目录
    > 此处用户可以使用xftp或者xshell等工具，书要简言

- 解压
    ```shell
    cd /home/linesno/zookeeper
    tar -zxvf zookeeper-3.4.6.tar.gz  # 解压之后zk路径为 /home/linesno/zookeeper/zookeeper-3.4.6
    ```
- 环境变量配置
    创建日志和数据目录
    ```shell
    mkdir -p /home/linesno/zookeeper/data   # 数据路径
    mkdir -p /home/linesno/zookeeper/logs   # 日志路径
    ```
- 修改配置
    ```shell
    # 复制默认配置为zoo.cfg
    cd /home/linesno/zookeeper/zookeeper-3.4.6/conf/
    cp zoo_sample.cfg zoo.cfg
    ```
    修改以下两项
    ```
    vim /home/linesno/zookeeper/zookeeper-3.4.6/conf/zoo.cfg

    # 修改以下内容
    dataDir=/home/linesno/zookeeper/data
    dataLogDir=/home/linesno/zookeeper/data
    ```

- 启动zk
    ```shell
    cd /home/linesno/zookeeper/zookeeper-3.4.6/bin   # 进入zk目录

    ./zkServer.sh start   # 启动zk日志
    ./zkServer.sh status  # 查看启动状态
    ```

## 集群安装
> zk集群安装只能为2n+1单数(如1/3/5)，集群数量不能为偶数

- 环境变量
- zk配置
- 集群启动

## Ansible构建

## 镜像
- 构建镜像
- 使用

## 参考资料
- [GitBook官网](http://www.baidu.com)
