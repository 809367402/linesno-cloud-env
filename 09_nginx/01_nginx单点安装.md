# Nginx单点教程
### 软件安装
#### 上传软件至Linux服务器`/home/linesno/soft/`目录
> 此处用户可以使用xftp或者xshell等工具，书要简言

#### 编译安装
安装依赖包
```bash
yum -y install openssl openssl-devel
yum -y install pcre pcre-devel
```

安装nginx
```bash
# 解压
tar -zxvf ~/soft/nginx-1.14.1.tar.gz -C ~/nginx/
cd /home/linesno/nginx/nginx-1.14.1

# 安装
./configure --prefix=/home/linesno/nginx --with-stream
make
make install
# 删掉源码包
rm -rf ~/nginx/nginx-1.14.1/
```

配置nginx
```bash
cd /home/linesno/nginx
# 创建http应用配置目录
mkdir -p conf/http.conf
# 创建tcp转发配置目录
mkdir -p conf/tcp.conf
```

编辑`nginx.conf`文件:
```bash
vim ~/nginx/conf/nginx.conf
```

调整以下内容
```bash
# 修改user
user linesno

# 打开日志格式
log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                  '$status $body_bytes_sent "$http_referer" '
                  '"$http_user_agent" "$http_x_forwarded_for"';

# 添加socket配置
include ~/nginx/conf/tcp.conf/*.conf;

# 添加配置
include ~/nginx/conf/http.conf/*.conf;
```

如下图:
<p align="center"><img src="/images/nginx_01.png" width="100%" /></p>

### 应用启动

启动命令
```bash
# 启动命令
sudo ~/nginx/sbin/nginx

# 重新加载配置
sudo ~/nginx/sbin/nginx -s reload
```

浏览器访问,输入网址[http://192.168.1.110](http://192.168.1.110)
<p align="center"><img src="/images/nginx_02.png" width="100%" /></p>

## Ansible构建
- 脚本编写

## 镜像
- 构建镜像
- 使用

## 参考资料
- [GitBook官网](http://www.baidu.com)

