# Nexus安装及权限配置

## Nexus安装
### 上传安装
上传Jenkins至Linux服务器`/home/linesno/soft`目录
> 此处用户可以使用xftp或者xshell等工具，书要简言

```bash
示例: scp -r nexus-2.14.1-01-bundle.tar.gz linesno@192.168.1.110:~/soft/
```

安装nexus
```bash
# 创建目录
mkdir -p ~/nexus
tar -zxvf ~/soft/nexus-2.14.1-01-bundle.tar.gz -C ~/nexus/
```

> Nexus的私服库会默认安装在~/.m2目录下面，此处我们使用的默认的即可

启动nexus
```bash
cd ~/nexus/nexus-2.14.1-01/bin
export RUN_AS_USER=root
./nexus start
```

访问http://192.168.1.110:8081/nexus ,出现如下界面:
<p align="center"><img src="/images/nexus_01.png" width="800px" /></p>

### 国内代理库

> 管理员账号:`admin/1234qwer`

> 开发人员账号:`deployment/1234qwer`(后面配置使用统一使用开发人员账号)

登陆nexus,默认密码`admin/admin1234`,修改密码为:`1234qwer`:
<p align="center"><img src="/images/nexus_02.png" width="800px" /></p>

修改开发人员密码
<p align="center"><img src="/images/nexus_06.png" width="800px" /></p>

添加阿里云库
<p align="center"><img src="/images/nexus_03.png" width="800px" /></p>

填写配置库配置信息,点击保存即可
```properties
Repository ID: Aliyun
Repository Name: Aliyun
Remote Storage Location: http://maven.aliyun.com/nexus/content/groups/public/
```
<p align="center"><img src="/images/nexus_04.png" width="800px" /></p>

将阿里云库添加到公共库中（公共库为对外统一库，maven连接使用的就是这个库)
<p align="center"><img src="/images/nexus_05.png" width="800px" /></p>

配置点击保存即可

## Ansible构建

## 镜像
- 构建镜像
- 使用

## 参考资料
- [GitBook官网](http://www.baidu.com)
