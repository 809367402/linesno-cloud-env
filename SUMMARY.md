# 目录
## 快速搭建环境
* [环境概要](80_base/README.md)
* [开发环境](80_base/20_开发环境搭建.md)
* [测试/准生产环境](80_base/22_准生产环境搭建.md)
* [生产环境](80_base/23_生产环境搭建.md)

## 软件管理
* [基线概要](README.md)
* [软件管理](00_soft/01_软件管理.md)

## 管理环境
* [CentOS安装](04_centos/02_CentOS安装.md)
    * [CentOS安装](04_centos/02_CentOS安装.md)
    * [CentOS配置](04_centos/03_CentOS配置.md)
* [GitBook安装](01_gitbook/01_GitBook安装.md)
    * [GitBook安装](01_gitbook/01_GitBook安装.md)
    * [Git安装](01_gitbook/02_Git安装.md)
* [邮件配置](02_email/01_邮件申请及开通客户端配置.md)
* [Jira安装和配置](03_Jira/01_Jira软件安装及破解.md)
* [Wiki管理](TEST.md)

## 基础软件
* [JDK安装](05_jdk/01_Linux的JDK配置.md)
    * [JDK安装配置](05_jdk/01_Linux的JDK配置.md)
    * [JVM监控](05_jdk/02_Jvm监控.md)
* [Redis安装](06_redis/01_Redis单点安装.md)
    * [Redis单点安装](06_redis/01_Redis单点安装.md)
    * [Redis集群安装](06_redis/02_Redis集群安装.md)
    * [RedisManager安装](06_redis/03_RedisManager安装配置.md)
* [Kafka安装](07_kafka/01_Kafka单点安装.md)
    * [Kafka单点安装](07_kafka/01_Kafka单点安装.md)
    * [Kafka集群配置](07_kafka/02_Kafka集群配置.md)
    * [KafkaManager安装](07_kafka/03_KafkaManager安装监控.md)
* [MySQL安装](08_mysql/01_MySQL单点安装.md)
    * [MySQL网络安装](08_mysql/04_MySQL网络安装.md)
    * [MySQL单点安装](08_mysql/01_MySQL单点安装.md)
    * [MySQL主从配置](08_mysql/02_MySQL主从配置.md)
    * [MyCAT安装](08_mysql/03_MyCAT安装.md)
* [Nginx安装](09_nginx/00_nginx配置规范.md)
    * [Nginx配置规范](09_nginx/00_nginx配置规范.md)
    * [Nginx单点安装](09_nginx/01_nginx单点安装.md)
    * [Nginx集群安装](09_nginx/02_nginx集群安装.md)
    * [KeepAlived安装](09_nginx/03_keepalived安装.md)

## 自动化平台环境
* [Nexus安装](14_nexus/01_Nexus安装配置.md)
    * [Nexus安装](14_nexus/01_Nexus安装配置.md)
    * [第三方jar管理](14_nexus/03_Nexus上传第三方jar包.md)
* [Maven安装和配置](24_maven/01_Maven本地安装.md)
    * [Maven在本地安装](24_maven/01_Maven本地安装.md)
    * [Maven服务器安装](24_maven/02_Maven服务器安装.md)
* [Jenkins安装](13_jenkins/01_Jenkins安装.md)
    * [Jenkins安装](13_jenkins/01_Jenkins安装.md)
    * [Jenkins插件安装配置](13_jenkins/01_Jenkins插件安装配置.md)
    * [Jenkins多节点构建](13_jenkins/02_Jenkins多节点构建.md)
    * [Jenkins多构建方式](13_Jenkins/03_Jenkins多构建方式.md)
* [Sonar安装](16_sonar/01_Sonar安装.md)
    * [Sonar安装](16_sonar/01_Sonar安装.md)
    * [Maven配置Sonar ](16_sonar/02_Maven配置Sonar.md)
* [Docker安装](TEST.md)
* [Harbor安装](17_harbor/01_Harbor单点安装.md)
    * [Harbor单点安装](17_harbor/01_Harbor单点安装.md)
    * [Harbor集群安装](17_harbor/02_Harbor集群安装.md)
    * [阿里云镜像仓库](17_harbor/03_阿里云镜像简单使用.md)
* [Kubernetes安装](TEST.md)
* [Zookeeper安装](11_zookeeper/01_Zookeeper单点安装.md)
    * [Zookeeper单点安装](11_zookeeper/01_Zookeeper单点安装.md)
    * [Zookeeper集群安装](11_zookeeper/02_Zookeeper集群安装.md)
    * [Zookeeper监控安装](11_zookeeper/03_Zookeeper监控安装.md)

## 运维监控
* [Ansible安装](19_ansible/01_Ansible源码安装.md)
    * [Ansible安装](19_ansible/01_Ansible源码安装.md)
    * [Ansible简单示例](19_ansible/02_Ansible配置无密钥登陆.md)
    * [AnsibleTower安装](19_ansible/03_AnsibleTower安装.md)
* [pinpoint安装](21_pinpoint/01_pinpoint安装配置.md)
    * [pinpoint单点安装](21_pinpoint/01_pinpoint安装配置.md)
    * [springcloud监控](21_pinpoint/02_springcloud监控.md)
* [skywalking安装](25_skywalking/01_skywalking安装配置.md)
    * [skywalking单点安装](25_skywalking/01_skywalking安装配置.md)
    * [springcloud监控](25_skywalking/02_springcloud监控.md)
* [ELK安装](22_elk/01_es安装.md)
    * [es安装](22_elk/01_es安装.md)
    * [logstash安装](22_elk/02_logstash安装.md)
    * [kibana安装](22_elk/03_kibana安装.md)
* [Zabbix安装和配置](TEST.md)

## 示例
* [发布和监控第一个工程](TEST.md)
